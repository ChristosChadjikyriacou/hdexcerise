package com.christos.chadjikyriacou.hd_excerise;

import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.*;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.floodAreaCheckerService.FloodAreaCheckerService;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.houseInsuranceAvailabilityCheckerService.HouseInsuranceAvailabilityCheckerService;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.houseInsuranceAvailabilityCheckerService.HouseInsuranceAvailabilityCheckerServiceImpl;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.subsidenceAreaCheckerService.SubsidenceAreaCheckerService;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodRisk;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class HouseInsuranceAvailabilityCheckerServiceTest {

    private static final String TEST_POST_CODE = "2660";
    private static final int TEST_LESS_NUMBER_OF_BEDROOMS = 3;
    private static final int TEST_MORE_NUMBER_OF_BEDROOMS = 5;
    private static final boolean TEST_HAS_NOT_THATCHED_ROOF = false;
    private static final boolean TEST_HAS_THATCHED_ROOF = true;
    private static final int TEST_MAX_NUMBER_OF_BEDROOMS_ALLOWED = 4;



    HouseInsuranceAvailabilityCheckerService houseInsuranceAvailabilityChecker;

    @Mock
    FloodAreaCheckerService floodAreaCheckerService;

    @Mock
    SubsidenceAreaCheckerService subsidenceAreaCheckerService;

    @Before
    public void initMocks(){
        houseInsuranceAvailabilityChecker = new HouseInsuranceAvailabilityCheckerServiceImpl(
                TEST_MAX_NUMBER_OF_BEDROOMS_ALLOWED,
                floodAreaCheckerService,
                subsidenceAreaCheckerService
        );
    }


    @Test
    public void whenNonSubsidenceAndLowFloodRiskAndLessThanMaxNumberOfBedroomsAndNotThatchedRoofReturnTrue() throws Exception {
        Mockito
                .when(floodAreaCheckerService.isPostcodeInFloodArea(TEST_POST_CODE))
                .thenReturn(FloodRisk.LOW_RISK);
        Mockito
                .when(subsidenceAreaCheckerService.isPostcodeInSubsidenceArea(TEST_POST_CODE))
                .thenReturn(false);

        Assertions.assertDoesNotThrow(()->{
            houseInsuranceAvailabilityChecker.checkAvailability(TEST_POST_CODE, TEST_LESS_NUMBER_OF_BEDROOMS, TEST_HAS_NOT_THATCHED_ROOF);
        });



    }

    @Test
    public void whenSubsidenceAndLowFloodRiskAndLessThanMaxNumberOfBedroomsAndNotThatchedRoofThrowsBusinessException() throws Exception {
        Mockito
                .when(floodAreaCheckerService.isPostcodeInFloodArea(TEST_POST_CODE))
                .thenReturn(FloodRisk.LOW_RISK);
        Mockito
                .when(subsidenceAreaCheckerService.isPostcodeInSubsidenceArea(TEST_POST_CODE))
                .thenReturn(true);


        Assertions.assertThrows(PropertyInSubsidenceAreaException.class,()-> {
            houseInsuranceAvailabilityChecker.checkAvailability(TEST_POST_CODE, TEST_LESS_NUMBER_OF_BEDROOMS, TEST_HAS_NOT_THATCHED_ROOF);
        });


    }


    @Test
    public void whenNonSubsidenceAndHighFloodRiskAndLessThanMaxNumberOfBedroomsAndNotThatchedRoofThrowsBusinessException() throws Exception {
        Mockito
                .when(floodAreaCheckerService.isPostcodeInFloodArea(TEST_POST_CODE))
                .thenReturn(FloodRisk.HIGH_RISK);
        Mockito
                .when(subsidenceAreaCheckerService.isPostcodeInSubsidenceArea(TEST_POST_CODE))
                .thenReturn(false);


        Assertions.assertThrows(PropertyHightRiskFloodAreaException.class,()-> {
            houseInsuranceAvailabilityChecker.checkAvailability(TEST_POST_CODE, TEST_LESS_NUMBER_OF_BEDROOMS, TEST_HAS_NOT_THATCHED_ROOF);
        });


    }

    @Test
    public void whenNonSubsidenceAndLowFloodRiskAndMoreThanMaxNumberOfBedroomsAndNotThatchedRoofThrowsBusinessException() throws Exception {
        Mockito
                .when(floodAreaCheckerService.isPostcodeInFloodArea(TEST_POST_CODE))
                .thenReturn(FloodRisk.LOW_RISK);
        Mockito
                .when(subsidenceAreaCheckerService.isPostcodeInSubsidenceArea(TEST_POST_CODE))
                .thenReturn(false);

        Assertions.assertThrows(InvalidNumberOfBedroomsException.class,()-> {
            houseInsuranceAvailabilityChecker.checkAvailability(TEST_POST_CODE, TEST_MORE_NUMBER_OF_BEDROOMS, TEST_HAS_NOT_THATCHED_ROOF);
        });

    }

    @Test
    public void whenNonSubsidenceAndLowFloodRiskAndLessThanMaxNumberOfBedroomsAndThatchedRoofThrowsBusinessException() throws Exception {
        Mockito
                .when(floodAreaCheckerService.isPostcodeInFloodArea(TEST_POST_CODE))
                .thenReturn(FloodRisk.LOW_RISK);
        Mockito
                .when(subsidenceAreaCheckerService.isPostcodeInSubsidenceArea(TEST_POST_CODE))
                .thenReturn(false);

        Assertions.assertThrows(PropertyHasThatchedRoofException.class,()-> {
            houseInsuranceAvailabilityChecker.checkAvailability(TEST_POST_CODE, TEST_LESS_NUMBER_OF_BEDROOMS, TEST_HAS_THATCHED_ROOF);
        });

    }


}
