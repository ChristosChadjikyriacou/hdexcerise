package com.christos.chadjikyriacou.hd_excerise;


import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.InsuranceNotAvailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical.ServiceUnavailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.subsidenceAreaCheckerService.SubsidenceAreaCheckerService;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.subsidenceAreaCheckerService.SubsidenceAreaCheckerServiceImpl;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.PostcodeNotFoundException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.subsidencewatch.SubsidenceAreaChecker;
import com.christos.chadjikyriacou.hd_excerise.uk.co.subsidencewatch.SubsidenceAreaCheckerTechnicalFailureException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SubsidenceAreaCheckerServiceTest {

    private static final String TEST_POST_CODE = "2660";


    SubsidenceAreaCheckerService subsidenceAreaCheckerService;

    @Mock
    SubsidenceAreaChecker subsidenceAreaChecker;

    @Before
    public void initMocks() {
        subsidenceAreaCheckerService = new SubsidenceAreaCheckerServiceImpl(subsidenceAreaChecker);
    }


    @Test
    public void whenPostcodeNotValidThenThrowBusinessException() throws Exception {
        Mockito
                .when(subsidenceAreaChecker.isPostcodeInSubsidenceArea(TEST_POST_CODE))
                .thenThrow(PostcodeNotFoundException.class);

        Assertions.assertThrows(InsuranceNotAvailableException.class,()-> {
            subsidenceAreaCheckerService.isPostcodeInSubsidenceArea(TEST_POST_CODE);
        });
    }

    @Test
    public void whenTechnicalFailureThenThrowTechnicalException() throws Exception {
        Mockito
                .when(subsidenceAreaChecker.isPostcodeInSubsidenceArea(TEST_POST_CODE))
                .thenThrow(SubsidenceAreaCheckerTechnicalFailureException.class);

        Assertions.assertThrows(ServiceUnavailableException.class,()-> {
            subsidenceAreaCheckerService.isPostcodeInSubsidenceArea(TEST_POST_CODE);
        });
    }

}
