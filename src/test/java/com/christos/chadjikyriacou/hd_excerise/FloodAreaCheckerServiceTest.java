package com.christos.chadjikyriacou.hd_excerise;

import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.InsuranceNotAvailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.PropertyInSubsidenceAreaException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical.ServiceUnavailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.floodAreaCheckerService.FloodAreaCheckerService;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.floodAreaCheckerService.FloodAreaCheckerServiceImpl;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodAreaChecker;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodAreaCheckerTechnicalFailureException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.PostcodeNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FloodAreaCheckerServiceTest {

    private static final String TEST_POST_CODE = "2660";


    FloodAreaCheckerService floodAreaCheckerService;

    @Mock
    FloodAreaChecker floodAreaChecker;

    @Before
    public void initMocks(){
        floodAreaCheckerService = new FloodAreaCheckerServiceImpl(floodAreaChecker);
    }

    @Test
    public void whenPostcodeNotValidThenThrowBusinessException() throws Exception {
        Mockito
                .when(floodAreaChecker.isPostcodeInFloodArea(TEST_POST_CODE))
                .thenThrow(PostcodeNotFoundException.class);

        Assertions.assertThrows(InsuranceNotAvailableException.class,()-> {
            floodAreaCheckerService.isPostcodeInFloodArea(TEST_POST_CODE);
        });
    }

    @Test
    public void whenTechnicalFailureThenThrowTechnicalException() throws Exception {
        Mockito
                .when(floodAreaChecker.isPostcodeInFloodArea(TEST_POST_CODE))
                .thenThrow(FloodAreaCheckerTechnicalFailureException.class);

        Assertions.assertThrows(ServiceUnavailableException.class,()-> {
            floodAreaCheckerService.isPostcodeInFloodArea(TEST_POST_CODE);
        });

    }

}

