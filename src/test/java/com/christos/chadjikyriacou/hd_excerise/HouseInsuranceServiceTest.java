package com.christos.chadjikyriacou.hd_excerise;

import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.PropertyInSubsidenceAreaException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.floodAreaCheckerService.FloodAreaCheckerService;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.houseInsuranceAvailabilityCheckerService.HouseInsuranceAvailabilityCheckerService;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.houseInsuranceService.HouseInsuranceService;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.houseInsuranceService.HouseInsuranceServiceImpl;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.InsuranceNotAvailableException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodRisk;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;


@RunWith(MockitoJUnitRunner.class)
public class HouseInsuranceServiceTest {

    private static final String TEST_POST_CODE = "2660";
    private static final int TEST_NUMBER_OF_BEDROOMS = 1;
    private static final boolean TEST_HAS_THATCHED_ROOF = false;
    private static final int TEST_PREMIUM = 110000;
    private static final int TEST_HIGH_RISK_FEE_PERCENTAGE = 15;
    private static final int TEST_INCREASED_PREMIUM = 126500;



    HouseInsuranceService houseInsuranceService;

    @Mock
    FloodAreaCheckerService floodAreaCheckerService;

    @Mock
    HouseInsuranceAvailabilityCheckerService houseInsuranceAvailabilityCheckerService;


    @Before
    public void initMocks(){
        houseInsuranceService = new HouseInsuranceServiceImpl(
                TEST_HIGH_RISK_FEE_PERCENTAGE,
                TEST_PREMIUM,
                floodAreaCheckerService,
                houseInsuranceAvailabilityCheckerService
        );

    }

    @Test
    public void whenNonSubsidenceAndLowFloodRiskThenReturnPremium() throws Exception {
        Mockito
                .when(floodAreaCheckerService.isPostcodeInFloodArea(TEST_POST_CODE))
                .thenReturn(FloodRisk.LOW_RISK);



        int calculatedPremium = houseInsuranceService.getCalculatedPremium(TEST_POST_CODE, TEST_NUMBER_OF_BEDROOMS, TEST_HAS_THATCHED_ROOF);
        Assertions.assertEquals(calculatedPremium, TEST_PREMIUM);

    }

    @Test
    public void whenNonSubsidenceAndMediumFloodRiskThenReturnIncreasedPremium() throws Exception {
        Mockito .when(floodAreaCheckerService.isPostcodeInFloodArea(TEST_POST_CODE))
                .thenReturn(FloodRisk.MEDIUM_RISK);

        int calculatedPremium = houseInsuranceService.getCalculatedPremium(TEST_POST_CODE, TEST_NUMBER_OF_BEDROOMS, TEST_HAS_THATCHED_ROOF);
        Assertions.assertEquals(calculatedPremium,TEST_INCREASED_PREMIUM);

    }


    @Test
    public void whenNonSubsidenceAndHighFloodRiskThenThrowBusinessException() throws Exception {
        Mockito .when(floodAreaCheckerService.isPostcodeInFloodArea(TEST_POST_CODE))
                .thenReturn(FloodRisk.HIGH_RISK);

        Mockito .doThrow(InsuranceNotAvailableException.class)
                .when(houseInsuranceAvailabilityCheckerService)
                .checkAvailability(TEST_POST_CODE, TEST_NUMBER_OF_BEDROOMS, TEST_HAS_THATCHED_ROOF);


        Assertions.assertThrows(InsuranceNotAvailableException.class,()-> {
            int calculatedPremium = houseInsuranceService.getCalculatedPremium(TEST_POST_CODE, TEST_NUMBER_OF_BEDROOMS, TEST_HAS_THATCHED_ROOF);
        });
    }
}
