package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness;

import java.util.Optional;

public class PropertyHasThatchedRoofException extends InsuranceNotAvailableException {


    public PropertyHasThatchedRoofException(){
        setReason(Optional.of("Property has thatched roof"));

    }

}
