package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness;

import java.util.Optional;

public class PropertyInSubsidenceAreaException extends InsuranceNotAvailableException {

    public PropertyInSubsidenceAreaException() {
        setReason(Optional.of("Property in subsidence area"));
    }
}
