package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness;

import java.util.Optional;

public class PropertyHightRiskFloodAreaException extends InsuranceNotAvailableException {


    public PropertyHightRiskFloodAreaException() {
        setReason(Optional.of("Property in high risk flooding area"));
    }

}
