package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.houseInsuranceService;

import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.InsuranceNotAvailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical.ServiceUnavailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.floodAreaCheckerService.FloodAreaCheckerService;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.houseInsuranceAvailabilityCheckerService.HouseInsuranceAvailabilityCheckerService;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodAreaChecker;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodRisk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class HouseInsuranceServiceImpl implements HouseInsuranceService{

    private final FloodAreaCheckerService floodAreaCheckerService;
    private final HouseInsuranceAvailabilityCheckerService houseInsuranceAvailabilityCheckerService;
    private final Integer baseFee;
    private final Integer highRiskPercentage;



    @Autowired
    public HouseInsuranceServiceImpl(
            @Value("{$house_insurance.high_risk_percentage}") Integer highRiskPercentage,
            @Value("${house_insurance.base_fee}") Integer baseFee,
            FloodAreaCheckerService floodAreaCheckerService,
            HouseInsuranceAvailabilityCheckerService houseInsuranceAvailabilityCheckerService) {
        this.baseFee = baseFee;
        this.highRiskPercentage = highRiskPercentage;
        this.floodAreaCheckerService = floodAreaCheckerService;
        this.houseInsuranceAvailabilityCheckerService = houseInsuranceAvailabilityCheckerService;
    }



    @Override
    public int getCalculatedPremium(String postcode, int numBedrooms, boolean hasThatchedRoof) throws ServiceUnavailableException, InsuranceNotAvailableException {
        houseInsuranceAvailabilityCheckerService.checkAvailability(postcode,numBedrooms,hasThatchedRoof);


        if (FloodRisk.MEDIUM_RISK == floodAreaCheckerService.isPostcodeInFloodArea(postcode)) {
            return baseFee + (baseFee * highRiskPercentage) / 100;
        }

        return baseFee;

    }
}
