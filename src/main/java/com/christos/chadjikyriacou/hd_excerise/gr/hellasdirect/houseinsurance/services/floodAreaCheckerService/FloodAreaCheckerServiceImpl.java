package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.floodAreaCheckerService;


import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.InsuranceNotAvailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical.ServiceUnavailableException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodAreaChecker;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodAreaCheckerTechnicalFailureException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodRisk;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.PostcodeNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FloodAreaCheckerServiceImpl implements FloodAreaCheckerService{

    FloodAreaChecker floodAreaChecker;

    @Autowired
    public FloodAreaCheckerServiceImpl(FloodAreaChecker floodAreaChecker) {
        this.floodAreaChecker = floodAreaChecker;
    }

    @Override
    public FloodRisk isPostcodeInFloodArea(String postcode) throws ServiceUnavailableException,InsuranceNotAvailableException {
        try {
            return floodAreaChecker.isPostcodeInFloodArea(postcode);
        }
        catch (FloodAreaCheckerTechnicalFailureException ex) {
            throw new ServiceUnavailableException();
        }
        catch (PostcodeNotFoundException ex) {
            throw new InsuranceNotAvailableException();
        }
    }
}
