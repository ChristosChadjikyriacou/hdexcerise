package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness;

import org.springframework.beans.factory.annotation.Value;

import java.util.Optional;

public class InvalidNumberOfBedroomsException extends InsuranceNotAvailableException {

    @Value("${house_insurance.max_number_of_bedrooms_allowed}")
    private Integer maxNumberOfBedroomsAllowed;


    public InvalidNumberOfBedroomsException(){
        if (maxNumberOfBedroomsAllowed != null) {
            setReason(Optional.of(String.format("The property must have less than %d rooms",maxNumberOfBedroomsAllowed)));
        }

    }

}
