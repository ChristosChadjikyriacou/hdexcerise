package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.floodAreaCheckerService;

import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.InsuranceNotAvailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical.ServiceUnavailableException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodRisk;

public interface FloodAreaCheckerService {

    public FloodRisk isPostcodeInFloodArea(String postcode) throws InsuranceNotAvailableException, ServiceUnavailableException;
}
