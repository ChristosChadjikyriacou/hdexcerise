package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.houseInsuranceAvailabilityCheckerService;

import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.*;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical.ServiceUnavailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.floodAreaCheckerService.FloodAreaCheckerService;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.subsidenceAreaCheckerService.SubsidenceAreaCheckerService;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodRisk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class HouseInsuranceAvailabilityCheckerServiceImpl implements HouseInsuranceAvailabilityCheckerService {


    private final FloodAreaCheckerService floodAreaCheckerService;
    private final SubsidenceAreaCheckerService subsidenceAreaCheckerService;


    private final Integer maxNumberOfBedroomsAllowed;

    @Autowired
    public HouseInsuranceAvailabilityCheckerServiceImpl(
            @Value("${house_insurance.max_number_of_bedrooms_allowed}") Integer maxNumberOfBedroomsAllowed,
            FloodAreaCheckerService floodAreaCheckerService,
            SubsidenceAreaCheckerService subsidenceAreaCheckerService) {
        this.maxNumberOfBedroomsAllowed = maxNumberOfBedroomsAllowed;
        this.floodAreaCheckerService = floodAreaCheckerService;
        this.subsidenceAreaCheckerService = subsidenceAreaCheckerService;
    }


    @Override
    public void checkAvailability(String postcode, int numBedrooms, boolean hasThatchedRoof) throws InsuranceNotAvailableException,ServiceUnavailableException {

        if (maxNumberOfBedroomsAllowed == null || postcode == null) {
            throw new ServiceUnavailableException();
        }

        if (numBedrooms > maxNumberOfBedroomsAllowed) {
            throw new InvalidNumberOfBedroomsException();
        }

        if (hasThatchedRoof) {
            throw new PropertyHasThatchedRoofException();
        }

        if (subsidenceAreaCheckerService.isPostcodeInSubsidenceArea(postcode)) {
            throw new PropertyInSubsidenceAreaException();
        }

        if (FloodRisk.HIGH_RISK == floodAreaCheckerService.isPostcodeInFloodArea(postcode)) {
            throw new PropertyHightRiskFloodAreaException();
        }





    }
}
