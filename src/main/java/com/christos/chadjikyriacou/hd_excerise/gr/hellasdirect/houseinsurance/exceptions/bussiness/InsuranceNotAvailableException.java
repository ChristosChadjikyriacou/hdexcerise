package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness;

import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
@Setter
public class InsuranceNotAvailableException extends RuntimeException {

    private Optional<String> reason;


    @Override
    public String getMessage() {

        String message = "Insurance Not Available";

        if (reason.isPresent()) {
            return message + "Reason: " + reason;
        }

        return  message;

    }
}
