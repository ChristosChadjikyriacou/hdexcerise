package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.houseInsuranceService;

import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.InsuranceNotAvailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical.ServiceUnavailableException;

public interface HouseInsuranceService
{
  public int getCalculatedPremium(String postcode, int numBedrooms, boolean hasThatchedRoof) throws ServiceUnavailableException, InsuranceNotAvailableException;
}