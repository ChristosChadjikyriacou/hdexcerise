package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.subsidenceAreaCheckerService;

import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.InsuranceNotAvailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical.ServiceUnavailableException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.PostcodeNotFoundException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.subsidencewatch.SubsidenceAreaChecker;
import com.christos.chadjikyriacou.hd_excerise.uk.co.subsidencewatch.SubsidenceAreaCheckerTechnicalFailureException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubsidenceAreaCheckerServiceImpl implements SubsidenceAreaCheckerService {

    @Autowired
    SubsidenceAreaChecker subsidenceAreaChecker;

    public SubsidenceAreaCheckerServiceImpl(SubsidenceAreaChecker subsidenceAreaChecker) {
        this.subsidenceAreaChecker = subsidenceAreaChecker;
    }

    @Override
    public Boolean isPostcodeInSubsidenceArea(String postcode) throws InsuranceNotAvailableException,ServiceUnavailableException {
        try {
            return subsidenceAreaChecker.isPostcodeInSubsidenceArea(postcode);
        }
        catch (SubsidenceAreaCheckerTechnicalFailureException ex) {
            throw  new ServiceUnavailableException();
        }
        catch (PostcodeNotFoundException ex) {
            throw new InsuranceNotAvailableException();
        }
    }
}
