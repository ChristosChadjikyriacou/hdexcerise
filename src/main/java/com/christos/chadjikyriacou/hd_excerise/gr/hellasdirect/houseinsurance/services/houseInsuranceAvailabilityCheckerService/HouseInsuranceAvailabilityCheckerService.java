package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.houseInsuranceAvailabilityCheckerService;

import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.InsuranceNotAvailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical.ServiceUnavailableException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.FloodAreaCheckerTechnicalFailureException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.floodwatch.PostcodeNotFoundException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.subsidencewatch.SubsidenceAreaCheckerTechnicalFailureException;

public interface HouseInsuranceAvailabilityCheckerService {
    /**
     * Checks if insurance is available depending on the current postcode , number of bedrooms and the if the property has thatched roof.
     *
     * @param postcode the postcode to assess
     * @param numBedrooms number of bedrooms of the access
     * @param hasThatchedRoof indicator for the thatched roof
     * @return boolean indicating if the property is available for insurance
     */
    public void checkAvailability(String postcode, int numBedrooms, boolean hasThatchedRoof) throws InsuranceNotAvailableException, ServiceUnavailableException;
}
