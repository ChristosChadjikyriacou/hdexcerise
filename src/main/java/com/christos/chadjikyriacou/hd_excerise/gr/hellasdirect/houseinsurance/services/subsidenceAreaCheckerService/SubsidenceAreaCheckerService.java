package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.services.subsidenceAreaCheckerService;

import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.bussiness.InsuranceNotAvailableException;
import com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical.ServiceUnavailableException;
import com.christos.chadjikyriacou.hd_excerise.uk.co.subsidencewatch.SubsidenceAreaCheckerTechnicalFailureException;

public interface SubsidenceAreaCheckerService {

    public Boolean isPostcodeInSubsidenceArea(String postcode) throws ServiceUnavailableException, InsuranceNotAvailableException;
}
