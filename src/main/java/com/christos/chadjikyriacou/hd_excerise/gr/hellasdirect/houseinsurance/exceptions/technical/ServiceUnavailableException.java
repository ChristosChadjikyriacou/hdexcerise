package com.christos.chadjikyriacou.hd_excerise.gr.hellasdirect.houseinsurance.exceptions.technical;

public class ServiceUnavailableException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Service Unavailable";
    }
}
