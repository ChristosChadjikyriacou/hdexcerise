package com.christos.chadjikyriacou.hd_excerise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HdExceriseApplication {

    public static void main(String[] args) {
        SpringApplication.run(HdExceriseApplication.class, args);
    }

}
